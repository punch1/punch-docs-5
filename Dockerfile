# Stage 1: Base image.
## Start with a base image containing NodeJS so we can build Docusaurus.
FROM node:lts as base
## Disable colour output from yarn to make logs easier to read.
ENV FORCE_COLOR=0
## Enable corepack.
RUN corepack enable
## Set the working directory to `/app/website`.
WORKDIR /app/website

# Stage 2: Production build mode.
FROM base as prod
## Set the working directory to `/app/website`.
WORKDIR /app/website
## Copy over the source code.
COPY . .
## Install dependencies with `--immutable` to ensure reproducibility.
RUN npm ci
## Build the static site.
RUN npm run build

# Stage 3: Serve with `docusaurus serve`.
FROM prod as serve
WORKDIR /app/website
## Expose the port that Docusaurus will run on.
EXPOSE 3000
## Run the production server.
CMD ["npm", "run", "serve"]