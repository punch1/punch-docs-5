module.exports = {
    docs: [
        {
            label: 'Introduction',
            collapsed: false,
            type: 'category',
            items: [
                'introduction/welcome',
                'introduction/so-youre-new-to-the-cia',
                'introduction/technical-details',
                'introduction/installation',
            ],
        },
        {
            label: 'Development',
            collapsed: false,
            type: 'category',
            items: [
                'development/getting-started',
                'development/style-guide',
            ],
        },
        {
            label: 'Server',
            collapsed: false,
            type: 'category',
            items: [
                'server/mail-server',
            ],
        },
        {
            label: 'Other',
            collapsed: false,
            type: 'category',
            items: [
                'other/project-setup',
                'other/docs-style-guide',
                'other/most-used-commands'
            ],
        },
    ]
};
