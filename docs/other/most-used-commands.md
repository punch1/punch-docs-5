---
id: most-used-commands
title: Most Used Commands
---

This page gives an overview of the most used command when developing with Symfony. It is a good idea to familiarize
yourself with these as they will save you a lot of time.

:::tip
You can find more information about the commands with --help, or view the symfony docs.

```
symfony console debug:router --help
```

:::

## Running the website
```bash
docker-compose up # Starts docker which hosts your database (no need to do it again if it's still running)
symfony server:start # Starts the Symfony web server
yarn watch # Starts webpack to compile your assets on changes
```

## Debug

### View all available commands

Shows an overview of all available commands.

```bash
symfony console list
```

### Debug the router

Shows an overview of all routes that are active. Very useful when you suspect there are multiple controllers listening
to the same route.

```bash
symfony console debug:router

----------------------------- -------- -------- ------ ------------------------------------------------ 
 Name                          Method   Scheme   Host   Path                                            
----------------------------- -------- -------- ------ ------------------------------------------------        
 app_login                     ANY      ANY      ANY    /login                                          
 app_logout                    ANY      ANY      ANY    /logout                                         
 admin                         ANY      ANY      ANY    /admin                                          
 cms_render_menu               ANY      ANY      ANY    /cms/render/menu                                                        
----------------------------- -------- -------- ------ ------------------------------------------------ 
```

### Clear the cache

It is sometimes needed to clear the cache when new security roles are introduced.

```bash
symfony console cache:clear
```

## Autogenerating code

### Make a CRUD Controller for the admin panel

Automatically generate a CRUD controller for the EasyAdmin panel. This is the preferred way to let users edit entities.

```bash
symfony console make:admin:crud
```

### Make a new console command

Generates a new console command that can be executed, much like all commands listed here!

```bash
symfony console make:command
```

### Make a new controller class

Generates an empty controller in a specified location. Saves you some time while developing.

```bash
symfony console make:controller
```

### Make a new CRUD controller

Generates all CRUD actions and forms for a specified entity.
:::note
It is almost always better to use `make:admin:crud` instead, as most CRUD operations should take place in the
admin panel.
:::

```bash
symfony console make:crud
```

### Make a new Entity

You should always use this command to create a new database Entity. It brings up an interactive menu where you can keep
adding fields to your entity. It makes relations much easier to implement.

```bash
symfony console make:entity
```

### Make new database Fixtures

The preferred way to populate your database. This command creates an empty fixture class for you.

```bash
symfony console make:fixtures
```

## Database

### Load all data fixtures

Clears your database and loads all fixtures defined in `src/DataFixtures`. This is the preferred way to populate your
database.

```sh
symfony console doctrine:fixtures:load
```

:::tip
Run the command with `-n` to avoid having to confirm clearing the database.
:::

### Make a new database migration

Creates a new migration based on all database changes.

```bash
symfony console make:migration
```

### Execute all database migrations

Brings your database up to date with all registered migrations. Can also be used to migrate up and down to your liking.

```bash
symfony console doctrine:migrations:migrate
```

