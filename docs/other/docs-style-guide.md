---
id: docs-style-guide
title: Docs Style Guide
---

This page describes the style used in the docs to maintain consistency and readability.
Most of these 'rules' are preferences and there is no real reason to enforce one particular style over another.
It is however important to enforce one style.

It assumes you are familiar with Markdown and Docusaurus. If not, please check the [Docusaurus documentation](https://v2.docusaurus.io/docs/markdown-features).

It is nice to start with a short introduction of what the page is about.
Always start with text instead of a markdown header of some sort for clarity.

If you got send to this page by a merge request reviewer there is no need to be personally offended.
It is likely there is a small inconsistency in your doc that you were unaware of.

## Sections
Please use H2 headers (`##`) for sections as H1 headers clash with the doc title. They also show up nice in the sidebar.

## Variables in text
If your doc depends on information that the user needs to provide. Please use the following format: `<VARIABLE_NAME>`

Example:
- Change the file to `<CURRENT_YEAR>-default.json`

## Embedding code snippets
Code can be inlined with single backticks \`like this\` and it shows up `like this`.

Multi-line code is embedded with three backticks and the code language:

    ```bash
    sudo rm -rf ~/all/the/bugs.css
    ```

becomes
```bash
sudo rm -rf ~/all/the/bugs.css
```

## File or directory names
Inline file or directory from the root directory: `src/Controller/Account/SecurityController`

## Admonitions
You can use admonitions like
```
:::note

This is a note

:::
```

to get

:::note

This is a note

:::

:::tip

This is a tip

:::

:::important

This is important

:::

:::caution

This is a caution

:::

:::warning

This is a warning

:::