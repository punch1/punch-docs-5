---
id: project-setup
title: Project Setup
---

This page is a comprehensive report of how the Punch site was set up.
If you've missed the introductory meeting, it's a good idea to read this page carefully.

## Setting up

Create a new Symfony Project with the following command. The full indicates we want to create a full website, so it includes frontend bundles like twig.
```
symfony new punch-symfony-5 --full
```

If we now open this project in PhpStorm, we can see the files and folders that were created.

- `/bin` Useful binaries live here. We will be using them all the time with `symfony console ...`
- `/config` Contains mostly .yaml files to configure bundles.
- `/migrations` A migration is a change to your database. Think of it as version control for your database.
- `/src` All our code (controllers, entities, and services) will go here.
- `/templates` Contains all html twig files.
- `/tests` For automated unit tests.
- `/translations` Translation files used to allow multiple languages.
- `/var` Cache and logs, you won't ever need to modify it.
- `/vendor` Our dependencies, will be installed with `composer install`.
- `.env` A file that describes environment variables. This one will be committed.
- `.env.local` Overwrites `.env` with local variables, like your local database credentials.


Note how `/var`, `/vendor` and `.env.local` are in the .gitignore. We will not commit these.

Symfony automatically creates a git repository. All we need to do is to add our PhpStorm `.idea` files to the `.gitignore`. 

Now we can start the site with
```
symfony server:start
```

We will be greeted by the default homepage. There are also some errors, so let's change that!

## Creating a homepage

We get the default Symfony homepage because we haven't configured our own. So let's do that.

Remember the `bin/console` from earlier? It has tons of commands that can help us create code.
The symfony CLI has a nice wrapper around it that takes into account your Symfony settings.
Run `symfony console list` to see them all.

For now, we will create a new `Controller`. A controller processes requests at a route, and returns a response.
```
symfony console make:controller
```
Call it `HomePageController`.

If we now check our files, we will see that it has created a `HomePageController.php`, and an `index.html.twig`.
We can see that the `HomePageController.php` renders the `index.html.twig`.
However, when we refresh our homepage, nothing has changed. Why is that? Because we haven't properly set the route on the controller!

In `HomePageController`, we can see the following Annotation
```php
    /**
     * @Route("/home/page", name="home_page")
     */
    public function index(): Response
```

This route is setup for `/home/page`, but we want it to match with `/`.
The name is fine, it is used to easily create references to this route, without needing to know the actual route.

Now that we've changed the route to `/`, we will see that our homepage has changed.
The default message is still kind of boring, so let's change that now!

## Changing the homepage
If we open the `home_page/index.html.twig` we've just created. We will see that it extends `base.html.twig`.
This is a very powerful feature. If we create a good-looking `base.html.twig`, we can extend it in all other templates to create a consistent layout of the site.

:::note
If you're used to a component architecture, it might take some time to get used to Twig. But don't worry, you can still include other files!
:::

In `base.html.twig`, we have defined a `block`:
```html
        {% block body %}{% endblock %}
```

When extending this file, we can override what we want to display in this block. Let's change our `index.html.twig` to:

```html
{% extends 'base.html.twig' %}

{% block title %}Home{% endblock %}

{% block body %}
    <p>Our very own homepage!</p>
{% endblock %}
```

And we have some custom text on our homepage. It's fairly empty, so lets see if we can display some data.

## Let's create some data
Our website if very empty. If we wanted to display static pages, we wouldn't need a framework like Symfony.
Let's create a basic version of news articles. We will create an `Entity`. This is a php class that can be stored in the database.

Something is broken, this fix is needed https://stackoverflow.com/questions/60618554/symfony-makeentity-annotation-mapping-error
```
symfony console make:entity Content\NewsArticle --force-annotation
```

:::danger
If you're on windows and it still doesn't work because it can't find the `patch` command. We can use the `patch.exe` from the Git binaries. They are usually located in `C:\Program Files\Git\usr\bin`, so [add this to your PATH](https://www.architectryan.com/2018/03/17/add-to-the-path-on-windows-10/)
:::

Add a `title` of type `string` and a `description` of type `string`, but have length 255 and can't be nullable.
Now Symfony has created an `Entity` as well as a `Repository`. The repository will be used later to retrieve entities from the database.

Let's actually setup a local database. Create a `.env.local` with the credentials to your local database.
```
DATABASE_URL="mysql://<USERNAME>:<PASSWORD>@127.0.0.1:3306/<DATABASE_NAME>?serverVersion=5.7"
```

And use symfony to create a new database
```
symfony console doctrine:database:create
```

Now we can make our first `Migration`. This takes our entities and maps them to SQL commands that alter the database.

```
symfony console make:migration
```

If you go to the `/migrations` folder, you can inspect exactly which commands this migration will perform.
Let's actually apply it with 
```
symfony console doctrine:migrations:migrate
```

Our database is still empty. We will use the amazing `DataFixtures` to generate dummy data for local development.
DataFixtures are simple scripts that populate your database. Let's install the bundle!
```
composer require --dev orm-fixtures
```

This generates an example fixture for us. Rename it to `NewsArticleFixtures.php` and add the following content.

```php
class NewsArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $news = new NewsArticle();
        $news->setTitle("Dummy article");
        $news->setContent("Lorem Ipsum");
        $manager->persist($news);

        $manager->flush();
    }
}
```

Now we can apply these fixtures with.
```
symfony console doctrine:fixtures:load
```

And we have our first data in the database!

## Displaying data

Let's render the news articles on the homepage.
First we have to update our `HomePageController` to retrieve the articles from the database and pass them to the twig template

```php
    /**
     * @Route("/", name="home_page")
     */
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getRepository(NewsArticle::class);

        return $this->render('home_page/index.html.twig', [
            'articles' => $em->findAll(),
        ]);
    }
```

And we can use twig to render the data on the homepage!

```html
{% block body %}
    <p>Our very own homepage!</p>

    <p>We have these news articles</p>

    {% for article in articles %}
        <div>
            <h4>{{ article.title }}</h4>
            <h2>{{ article.content }}</h2>
        </div>
    {% endfor %}
{% endblock %}
```

That's it! If we refresh, we can now see our article on the homepage!

## Make it look pretty
This is an ugly website. It works, but it does not look good. Let's add Symfony's front-end tool: Webpack Encore!

Webpack is a nice way to bundle all your front-end resources. It also supports live-reloading.

```
composer require symfony/webpack-encore-bundle
yarn install
```

The files that are generated mostly aren't interesting. The `package.json` manages the dependencies for the frontend.
The `webpack.config.js` is where we can configure Webpack (which we will be doing soon).
Uncomment the helpers in `base.html.twig` to activate it in all templates.

When working on the frontend, you'll need to run the encore dev server, which compiles the assets and allows for live-reloading.

```
yarn encore dev-server
```

We will now add [Tailwind](https://tailwindcss.com).
Tailwind is a frontend framework that supplies a bunch of classes to use instead of css.

We won't go into detail on how to add it, it's already written in the following tutorials.

https://dev.to/railto/using-tailwind-css-and-purgecss-with-symfony-encore-5cgj
https://tailwindcss.com/docs/installation#post-css-7-compatibility-build

Let's style our homepage

```html
    <div class="flex flex-col">
        {% for article in articles %}
            <div class="m-4 p-4 bg-blue-500 border-2 border-blue-600">
                <h4>{{ article.title }}</h4>
                <h2>{{ article.content }}</h2>
            </div>
        {% endfor %}
    </div>
```

Now it's pretty... enough

## Creating a Member
Symfony has excellent built-in authentication. We will now create a `Member` entity that can log in.

The Symfony docs are comprehensive, so no need to give an explanation here.

https://symfony.com/doc/current/security.html

Our `User`s will be called `Member`. Create a `MemberFixtures` with a Member and Admin

```php
<?php
class MemberFixtures extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $member = new Member();
        $member->setEmail("member");
        $member->setPassword($this->passwordEncoder->encodePassword(
            $member,
            'member'
        ));

        $manager->persist($member);

        $admin = new Member();
        $admin->setEmail("admin");
        $admin->setPassword($this->passwordEncoder->encodePassword(
            $admin,
            'admin'
        ));
        $admin->setRoles(['ROLE_ADMIN']);

        $manager->persist($admin);

        $manager->flush();
    }
}
```

## Creating a login form
Once again we will follow the Symfony docs: https://symfony.com/doc/current/security/form_login_setup.html

This automatically creates a login and logout route for us. Let's update the `base.html.twig` to allow us to log in.

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>{% block title %}Welcome!{% endblock %}</title>
    {% block stylesheets %}
        {{ encore_entry_link_tags('app') }}
    {% endblock %}

    {% block javascripts %}
        {{ encore_entry_script_tags('app') }}
    {% endblock %}
</head>
<body class="bg-gray-300">
<div class="p-4 bg-blue-600 justify-between flex flex-row shadow-md">
    <div class="text-4xl font-medium my-auto">Punch</div>
    <div class="float-right">
        <div class="flex flex-row">
            {% if app.user %}
                <div class="p-4 italic">{{ app.user.username }}</div>
                <a href="{{ path('app_logout') }}">
                    <div class="p-4 hover:text-white">Log out</div>
                </a>
            {% else %}
                <a href="{{ path('app_login') }}">
                    <div class="p-4 hover:text-white">Log in</div>
                </a>
            {% endif %}
        </div>
    </div>
</div>

<div class="m-4 p-4 bg-blue-600 shadow-lg">
    {% block body %}{% endblock %}
</div>
</body>
</html>
```

We can now log in and out!

## Create an admin backend
We don't want to use data fixtures in production. We need a way to create actual data on the website.
The admin bundle [EasyAdmin 3](https://symfony.com/doc/current/bundles/EasyAdminBundle/) is great. It automatically creates crud controllers for our entities.
Let's install it
```
composer require easycorp/easyadmin-bundle
```

And create our dashboard

```
symfony console make:admin:dashboard
```

Now it is very important that we secure the backend so only admins can access it.

```yaml title="config/packages/security.yaml"
    access_control:
      - { path: ^/admin, roles: ROLE_ADMIN }
```

Let's create our first crud controller for the `NewsArticle`s

```
symfony console make:admin:crud
```

Create a link to our newly create CrudController and add it to the dashboard.

```php title="src/Controller/Admin/DashboardController.php"
    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('News', 'fas fa-newspaper', NewsArticle::class);
    }
```

And presto, we can now create new NewsArticles!
