---
id: technical-details
title: Technical Details
---

This page will give a brief overview of the main tools used and provide tutorial links when necessary.

## Symfony
The website is built with Symfony, a powerful PHP framework that has a lot of functionality out of the box.
It might seem a bit overwhelming at first, but once you start to understand how it works the advantages of using it will show.

If you are unfamiliar with Symfony [this book](https://symfony.com/doc/current/the-fast-track/en/index.html) is a nice place to start.
It is written by the creator of Symfony. You can follow the chapters up to and including _Testing_.
We don't use Symfony cloud to deploy, so you can skip those chapters.

In case you need a refresher on HTML, CSS, JS or PHP, we recommended following their respective courses on [Codecademy](https://www.codecademy.com/).

## Mysql
[MySQL](https://www.mysql.com/) is an open-source relational database management system.
Fortunately you rarely have to interact with the database itself, because we use [Doctrine](#doctrine).

## Doctrine
[Doctrine](https://www.doctrine-project.org/) is an [ORM](https://en.wikipedia.org/wiki/Object-relational_mapping)
which generates the database schema based on the entities you declare in the code.
It abstracts the database, so you don't have to worry about it (as much) while developing.
A nice explanation is presented in the [Symfony docs](https://symfony.com/doc/current/doctrine.html).

## Composer
[Composer](https://getcomposer.org/) Composer is a tool for dependency management in PHP.
It allows you to declare the libraries your project depends on, and it will manage (install/update) them for you.

## EasyAdmin
[EasyAdmin](https://symfony.com/doc/current/bundles/EasyAdminBundle/index.html) is an excellent tool to create a nice backend.
It makes it trivial to create CRUD operations for Entities, so there is no need to create boilerplate forms.

## Git(Lab)
We use [Git](https://git-scm.com/) for version control.
We have chosen Git over [Mercurial](https://www.mercurial-scm.org/) as it has become the industry standard and our members have more experience with it.

Our repositories are hosted on [GitLab](https://gitlab.com/punch1). 

## Keepass
All our passwords are saved in a Keepass. If you have access to these docs, you should have access to the Keepass as well. 

## Recommended tools
These tools are not strictly required to develop. However, most members use them, and it is advised that you should too.

### Phpstorm
[PhpStorm](https://www.jetbrains.com/phpstorm/) is a clever PHP IDE developed by Jetbrains.
It features support for most web-oriented languages and has a useful Symfony plugin.
Get your (free) student license [here](https://www.jetbrains.com/shop/eform/students).

### Docker
[Docker](https://www.docker.com/) handles your development environment for you.
No need to configure your database or tinker with php modules anymore!
