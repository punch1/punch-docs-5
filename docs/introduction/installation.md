---
id: installation
title: Installation
---

This page will explain how to get started with developing the Punch website.
The process differs a bit for different operating systems.
If you run into any problems, make sure to take a look at the [Trouble Shooting](#troubleshooting) below.
In case your error is missing, please update the section once you resolve it!

Make sure to take a look at the [Official Symfony Documentation](https://symfony.com/doc/current/setup.html) to see the up-to-date requirements for running symfony.

:::tip
This page is intended to guide you through the installation process.
If you have already installed everything in a previous session, and you want to start/continue working on an issue, please use the [Getting started](../development/getting-started) page for guidance.
:::

## Requirements
If you need a reminder what all these tools do, make sure to check [Technical Details](./technical-details).
- [Php 8.1.x](https://www.php.net/downloads.php)
- [Git](https://git-scm.com/)
- [Composer 2](https://getcomposer.org/download/)
- [Symfony CLI](https://symfony.com/download)
- [Docker](https://www.docker.com/)
- [Node JS](https://nodejs.org/en/)
- [Yarn](https://classic.yarnpkg.com/en/docs/install/)

Check if everything is configured correctly with
```bash
symfony check:requirements
```

## Installation
Clone the punch site in a folder of your choosing:
```bash
git clone https://gitlab.com/punch1/punch-website-5
```

Install the php dependencies
```bash
composer install
```

Install the Javascript dependencies
```bash
yarn install --force
```

Start docker to host the database (-d to start in the background)
```bash
docker-compose up -d
```

:::caution
On many computers, opening Docker Desktop or running a `docker-compose` command raises errors.
If that happens, check the steps in the [Troubleshooting](#docker-desktop---install-wsl-2-kernel-update) section before you continue.
:::
Check the port that docker is hosting your database with and remember it for the next step
```bash
docker-compose ps
```

Create a `.env.local` in the root of the project with the following content. Replace the `<PORT>` with the port from the previous step
```bash
DATABASE_URL="mysql://main:main@127.0.0.1:<PORT>/punch?serverVersion=5.7"
```
This tells Symfony where it can find your database.

Update your database schema and load the fixtures:
```bash
symfony console doctrine:migrations:migrate
symfony console doctrine:fixtures:load
```
:::note
When you run the 2nd command, you will get the question `Careful, database "punch" will be purged. Do you want to continue? (yes/no) [no]`.
You can safely answer this with yes. It means all data will be removed from your local database. It will be filled again by the fixtures.
This ensures that everyone has the same local data.
You can also run the command with `--no-interaction` to skip this question!
:::
:::caution
If you get an exception along the lines of `Could not find driver`, check the [Troubleshooting](#could-not-find-driver).
:::

## Running the website
You will need to run 3 commands, in 3 separate terminals to start working on the website
```bash
docker-compose up # Starts docker which hosts your database (no need to do it again if it's still running)
symfony server:start # Starts the Symfony web server
yarn watch # Starts webpack to compile your assets on changes
```
That's it, now you can start working on the website! Your local repository is now up-to-date with the current master branch.
You can view it in your browser at https://127.0.0.1:8000/.

From now on, if you want to start/continue working on an issue or if you want to review a merge request, follow the steps in [Getting Started](../development/getting-started).

## Troubleshooting

### Could not find driver
This is a [missing php extension](#missing-php-extensions), make sure to enable `pdo_mysql`.

### Missing php extensions
Check which php version you are running:
```
symfony local:php:list
```
Go to the corresponding folder and open the `php.ini` file in there. If there is no `php.ini`, copy `php.ini-development` and rename it to `php.ini`.

In `php.ini`, uncomment the correct extensions (mentioned in the error message). Also, uncomment `extension_dir = "ext"`.

### Docker Desktop - Install WSL 2 kernel update
If you get this error message:
![wsl 2 installation is incomplete](./images/docker-wsl-2-incomplete.jpeg)
then your computer is missing an update package for WSL. WSL (Windows Subsystem for Linux) lets you run Linux applications on Windows, which is necessary for Docker.

To solve it, follow [the link](http://aka.ms/wsl2kernel). On the webpage, click the link to download the latest package and run the installer. Afterwards, restart your computer and check if you can open Docker Desktop without problems.

### Docker Desktop - Virtualization
If you get this error message:
![Enable virtualization](./images/docker-virtualization.png)
then follow these steps:
1. Open windows features (in Dutch: Windows-onderdelen in- of uitschakelen) and check the boxes for:
   - Virtual Machine Platform (in Dutch: Platform voor virtuele machine)
   - Windows Subsystem for Linux
2. In the BIOS menu, enable 'Virtualization Technology'. Accessing the BIOS menu can be different per computer. For most Windows 10 computers, you can use [this instruction](https://www.laptopmag.com/articles/access-bios-windows-10).

### Database update
If you have problems updating the database (e.g. because the update partially succeeded before), run these commands:
```bash
symfony console doctrine:database:drop --force
symfony console doctrine:database:create
symfony console doctrine:migrations:migrate --no-interaction
symfony console doctrine:fixtures:load --no-interaction
``` 
