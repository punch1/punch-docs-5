---
id: so-youre-new-to-the-cia
title: So You're New to the CIA?
---

So you're new to the CIA? Welcome aboard!

This page contains a step-by-step guide on how to get started and start contributing as soon as possible!

### Communication
- Join the Discord
- Enable mobile notifications to stay up to date.
- Look through `#links-and-resources`.
- Create a gitlab account, and post your username here, so we can grant you access to the code.

### Symfony Basics
- Read through the `Technical Details` doc, it describes all tools we use to
  develop. https://docs.punchvolleybal.nl/docs/introduction/technical-details
    - Install PhpStorm, get a free student license.
- If you are unfamiliar with some, read through the linked docs or ask us questions about it!
- Work through The Fast Track. A book that teaches the most important Symfony concepts and make sure to code along. It
  also explains what to install to run Symfony locally. Work up to and including `Building the User Interface` and
  you'll get comfortable with 90% of the functionality the Punch site has.

### Local Development
- Setup your local environment https://docs.punchvolleybal.nl/docs/introduction/installation
    - Choose whether you want to user Docker or local MySQL. If you don't know what this means, choose Docker.
- Look through the code `src/DataFixtures` to find the credentials to the local admin account.
- Checkout the `/admin` and play around with it.
- Make yourself Hero of the Week, you've earned it.
- Ping me once you're here to get assigned a nice introductory issue to work on.

### My First Issue
- Ping me once you're here to get assigned a nice introductory issue to work on.
- Read `Getting Started` to get familiar with the
  workflow. https://docs.punchvolleybal.nl/docs/development/getting-started
- Implement the issue, and open your first pull request.
- Get it rejected because we like to complain a lot.
- Implement reviewer feedback.
- :partying_face: First Pull Request merged :partying_face: 
