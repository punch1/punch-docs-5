---
id: welcome
title: Welcome! Everything is fine.
slug: /
---

Welcome to the documentation of the Punch website.
These docs should contain everything needed to start developing!

The docs are organized with the sidebar on the left which should be fairly straightforward.
There is also a blog which contains more opinionated information on major events instead of the cold, hard instructions of the docs.

In case you believe something is missing or could otherwise be improved, don't hesitate to open an issue in the [repository](https://gitlab.com/punch1/punch-docs-5).
Bonus points if you fix it yourself!

