---
id: style-guide
title: Style Guide
---

We use the recommended Symfony styles as listed in the [official documentation](https://symfony.com/doc/current/contributing/code/standards.html).

To enforce this style, we use [PHP CS Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer) which is a tool that automatically fixes most issues.

You can run it to fix all your problems
```bash
composer run fix-php
```

:::caution
Please run this command before you commit, as the CI will fail on style errors.
:::

:::tip
If you use PhpStorm, it will automatically detect and warn you of any mistakes, as well as provide you with options to quick-fix any errors.
:::

The most relevant rules:

- Always use strict comparison if possible (`===`)
- Use camelCase for PHP variables, function and method names, arguments (e.g. `$acceptableContentTypes`, `hasSession()`)
- Use snake_case for configuration parameters and Twig template variables (e.g. `framework.csrf_protection`
  , `http_status_code`)
- Use namespaces for all PHP classes and UpperCamelCase for their names (e.g. `ConsoleLogger`)
- Prefix all abstract classes with `Abstract`
- Use [Yoda conditions](https://en.wikipedia.org/wiki/Yoda_conditions) when checking a variable against an expression to
  avoid an accidental assignment inside the condition statement
- Declare class properties before methods
- Declare public methods first, then protected ones and finally private ones.
