---
id: getting-started
title: Getting Started
---

Before you can start developing, you always have to make sure your local repository is updated.
This holds not only for the code, but also for the database, packages, etc. In this section, we describe the steps that you have to follow each time you start to work on the Punch website.

Depending on the number and type of changes that have been done since your last commit, you might not need all commands below.
Therefore, each step contains a short explanation on what it does and when you need it.
If you are not sure (especially in the beginning), you can just execute all steps, so you are sure your local repository is up-to-date.

## Checking out the right branch/commit
When you open phpStorm to work on the website, there are a few possible scenarios. For each scenario, here is how to checkout the code you need.
:::note
There are many ways to update your code using Git. In this guide, we will use the Git menu and terminal within PhpStorm: ![PhpStorm Git menu](./images/phpstorm-git-menu.png)
If you feel more comfortable using another way, feel free to do so.
:::
### You start working on a new issue
1. Checkout the local master branch:
![Checkout master](./images/phpstorm-checkout-master.png)
2. Update your local master branch by running
```bash
git pull
```

3. Create a new local branch. Make sure the name tells other developers what you are working on

Now your code is up-to-date. Since you updated the files on your computer by pulling from master, you might have to execute (some of) the [other updates](#other-updates). 
Which steps to execute, depends on the changes that other people did on `master`, since you last pulled from it.

### You want to review a merge request
In this case, you just have to checkout the remote branch for which the merge request was created.
A local branch with the same name will be automatically created and the files on your computer will be updated.

To determine which [other updates](#other-updates) you have to do, look at the changes that were done on this new branch (the merge request gives a nice overview).

### You continue working on your own branch
If you open PhpStorm on the branch that you want to work on, you can start right away, as long as you are sure that no one else made changes.
You should be able to immediately [run your local website](../other/most-used-commands#running-the-website). 

To check if no one else made changes to your branch in the mean time, you can run
```bash
git pull
```
to see if there are any updates.
If this is the case, check the updated files to determine if you need to do [other updatess](#other-update-commands).

## Other update commands
In many situations, updating the code is not enough to be able to fully run your local website.
Always check if you need any of the following updates:

### Update dependencies
```bash
composer install
yarn install --force
```
This is necessary when there are changes in `composer.json`, `composer.lock` or `symfony.lock`.

### Update assets
```bash
yarn run watch
```
This is necessary when there are new `.css` or `.js` files.

### Update your database structure
```bash
docker-compose up -d
symfony console doctrine:migrations:migrate
```
This is necessary when there are new entries in the `/migrations` folder.
:::note
If this is not working, try to remove your `punch-website-5` database from Docker (using Docker Desktop) and run the commands again. If you do this, you also have to update the [data fixtures](#load-new-data-fixtures).
Another option to do this all at once is to run the commands in the [troubleshooting section](../introduction/installation#database-update).
:::

### Load new data fixtures
```bash
symfony console doctrine:fixtures:load
```
This is necessary when there are changes in the `src/DataFixtures` folder.

## Run your local website
Now you are ready to run the local website and start developing.
The commands to run the local website can be found in the section with [most used commands](../other/most-used-commands#running-the-website).
