---
id: mail-server
title: Mailing
---

Our mail processing is quite complex matter because several components are involved. 
Since there are many different scenarios, there are many different configuration. 
This document will provide some overview

Graphical overview of the set-up:
![Overview of the mail flow](./images/mail-flow.png)

### Components

These are the necessary components:
- Postfix as MTA (Mail Transfer Agent) –> This is the SMTP server. It accepts incoming mail and queries the transport 
directly from the database and passes it to Dovecot, Symfony or directly handles the alias. 
It forwards outgoing mail to the next responsible SMTP server.
 
- Dovecot as LDA (Local Delivery Agent) –> This is the IMAP server. It accepts incoming mail from Postfix and stores it in virtual mailboxes. 
It is connected to the database for authentication and lookups.
 
 - Symfony Console as `PunchTransporter` -> All mailinglists of `AbstractGroup` are handled within 
 Symfony. 
 
 - Amavis, ClaimAV & Spamassain -> Add magic headers like `X-SPAM-SCORE` to the message.
 Symfony and Dovecot use them for spam detection.
 
- Let's Encrypt as CA (Certificate Authority) -> Provides us free SSL/TLS certificates we can use for encrypting our mail traffic

### Transporting
The configurations allows for 4 types of transport
- (Virtual) Mailboxes -> This is used by for example the board. A mailbox is a place where emails get stored and can be received 
using an IMAP connection. Mailboxes can be created in Symfony. Postfix checks if the recipient is 
in the `virtual_mailbox` table in the database and continues with the `Virtual`Transporter. 

- Alias -> This is used for forwarding an alias to a single other address. The aliases are also handled in Postfix. 
Postfix checks if the recipient is in the `mail_alias` table in the database and forwards the mail to the destination also in this table.

- Mailing lists (or Distribution Lists) -> This is used for groups (like teams or committees). If an incoming mail is not picked up 
as __Mailbox__ or __Alias__, Postfix assigns the `PunchTransporter`. This is a custom transporter that pipes the message to the 
Symfony Console.

### Set-up your mailclient for a mailbox
Famous mailclients like Thunderbird, Gmail, Apple Mail or Outlook can be used to receive and send mails from the mailserver.
Most of the connection details will be automatically received from the server:

|                   |          IMAP:                    |  SMTP:                            |
|---                |---                                |---                                |
| Server            |   punch.tudelft.nl                |   punch.tudelft.nl                |
| Port              |   143                             |   25                              |
| SSL               |   STARTTLS                        |   STARTTLS                        |
| Authentication    |   Normal Password                 |   Normal Password                 |
| Username          |   user (without @punch.tudelft.nl)|   user (without @punch.tudelft.nl)|

### Async mailing
Async processes are running parallel of the symfony request so they don't lead to timeouts. Creating a async process in symfony can be done by mapping the function and datamessage within [symfony/messenger](https://symfony.com/doc/current/messenger.html). This async process is running on another thread using the supervisor process explained in the symfony docs.

The process itself is quite straightforward. When calling the function `sendMail()`, the mail is added to the queue (in the database) and the controller continues with executing the request without any timeout of the `sendMail()` function. In the background, the supervisor is running the `sendMail()` function on another thread on the server, in FIFO order.
