module.exports = {
  title: 'Punch Docs',
  tagline: 'Everything you need to know to work on the new Punch website',
  url: 'https://your-docusaurus-test-site.com',
  baseUrl: '/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'BCC',
  projectName: 'punch-docs',
  themeConfig: {
    navbar: {
      title: 'Punch Docs',
      logo: {
        alt: 'Punch Logo',
        src: 'img/punch-banner.png',
      },
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Docs',
          position: 'left',
        },
        {to: 'blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/punch1/punch-docs-5',
          label: 'GitLab',
          position: 'right',
        },
      ],
    },
    footer: {

    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/punch1/punch-docs-5/-/tree/master/',
        },
        blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://gitlab.com/punch1/punch-docs-5/-/tree/master/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
